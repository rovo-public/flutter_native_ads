import 'package:flutter/services.dart' as Service;
import 'package:flutter_test/flutter_test.dart';
import 'package:native_ads/native_ads.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  group('NativeAds', () {
    const Service.MethodChannel channel = Service.MethodChannel('native_ads');

    final List<Service.MethodCall> log = <Service.MethodCall>[];

    setUp(() async {
      TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger
          .setMockMethodCallHandler(channel, (message) {
        switch (message.method) {
          case 'initialize':
            return Future<bool>.value(true);
          default:
            assert(false);
            return null;
        }
      });

      test('initialize', () async {
        log.clear();
        expect(NativeAds.initialize(), isInstanceOf<NativeAds>());
        expect(log, <dynamic>[
          isMethodCall('initialize', arguments: null),
        ]);
      });

      tearDown(() {
        TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger
            .setMockMethodCallHandler(channel, (message) => null);
      });
    });
  });
}
